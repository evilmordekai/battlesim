﻿using System;
using System.Diagnostics;

namespace combat_simulator
{
    public enum DiceType {
        Green,
        Red,
        Blue
    };

    public class Die
    {
        public Die(DiceType _type ) { type = _type; }

        public bool roll()
        {
            int threshold = 0;
            switch (type)
            {
                case DiceType.Green: threshold = 2; break;
                case DiceType.Red: threshold = 3;  break;
                case DiceType.Blue: threshold = 4; break;
                default: Debug.Assert(false, "unknown defense die type"); throw new NotImplementedException();
            }
            return CombatManager.m_randomNumberGenerator.Next(threshold, 7) <= threshold; // simulate 6-sided die}
        }
        public DiceType type;
    }


}
