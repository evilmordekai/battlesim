﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace combat_simulator
{
    public enum Race
    {
        Dwarves, NightStalkers, Horde, Humans, Elves, Serpents
    };
    
    public class Army
    {
        public Army(CombatManager _manager) { Init(_manager);}

        public Army(CombatManager _mananger, Race race, int numBasic, int numAdvanced, int numSpecial)
        {
            Init(_mananger);
            m_race = race;
            for (int i = 0; i < numBasic; ++i)
                m_units[Unit.Type.Basic].Add(CreateUnit(Unit.Type.Basic, race));
            for (int i = 0; i < numAdvanced; ++i)
                m_units[Unit.Type.Advanced].Add(CreateUnit(Unit.Type.Advanced, race));
            for (int i = 0; i < numSpecial; ++i)
                m_units[Unit.Type.Special].Add(CreateUnit(Unit.Type.Special, race));

        }

        public void Init(CombatManager _manager)
        {
            m_combatManager = _manager;
            m_units = new Dictionary<Unit.Type, List<Unit>>();
            m_units.Add(Unit.Type.Basic, new List<Unit>());
            m_units.Add(Unit.Type.Advanced, new List<Unit>());
            m_units.Add(Unit.Type.Special, new List<Unit>());
        }

        public enum CombatPosition { Attack, Defense };

        public bool UnitsRemain()
        {
            foreach (List<Unit> units in m_units.Values)
                if (units.Count > 0)
                    return true;
            return false;
        }

        // returns number of successful attacks
        public int RollAttackDiceForCombat() { return RollDiceForCombat(CombatPosition.Attack, false); }
        public int RollDefenseDiceForCombat(bool mayAddDefenseDicePool) { return RollDiceForCombat(CombatPosition.Defense, mayAddDefenseDicePool); }
       
        private int RollDiceForCombat(CombatPosition position, bool mayAddDefenseDicePool)
        { 
            int numSuccesses = 0;

            // Defense Dice
            if (position == CombatPosition.Defense && mayAddDefenseDicePool)
            {
                numSuccesses += ComputeAndRollDefenseDice();
            }

            foreach (List<Unit> unitsOfType in m_units.Values)
            {
                foreach (Unit unit in unitsOfType)
                {
                    int rollResult = CombatManager.m_randomNumberGenerator.Next(1, 7); // simulate 6-sided die
                    int threshold = position == CombatPosition.Attack ? unit.attack : unit.defense;
                    if (rollResult <= threshold)
                        ++numSuccesses;
                }
            }


            if (m_race == Race.Elves) // Compute & roll Unity bonuses
            {
                int numUnityDice = 
                    Math.Min( Math.Min(m_units[Unit.Type.Basic].Count,
                                       m_units[Unit.Type.Advanced].Count),
                              m_units[Unit.Type.Special].Count);
                DiceType typeOfDie = DiceType.Green;

                for (int i = 0; i < numUnityDice; ++i)
                {
                    Die die = new Die(typeOfDie);
                    numSuccesses += die.roll() ? 1 : 0;
                }
            }

            return numSuccesses;
        }

        // computes number & type of Defense Die allotted to the defender, rolls them, and returns the rolled # of casualties.
        private int ComputeAndRollDefenseDice()
        {
            int totalNumDefenders =  GetTotalSize();

            uint numDiceToRoll = 0;
            DiceType typeOfDie = DiceType.Red;

            if (totalNumDefenders >= 1 && totalNumDefenders <= 3)
            {
                numDiceToRoll = Properties.Settings.Default.DDPNumberRangeA;
                typeOfDie = Properties.Settings.Default.DDPColorRangeA;
            }
            else if (totalNumDefenders >= 4 && totalNumDefenders <= 6)
            {
                numDiceToRoll = Properties.Settings.Default.DDPNumberRangeB;
                typeOfDie = Properties.Settings.Default.DDPColorRangeB;
            }
            else if (totalNumDefenders >= 7 && totalNumDefenders <= 9)
            {
                numDiceToRoll = Properties.Settings.Default.DDPNumberRangeC;
                typeOfDie = Properties.Settings.Default.DDPColorRangeC;
            }
            else if (totalNumDefenders >= 10 && totalNumDefenders <= 12)
            {
                numDiceToRoll = Properties.Settings.Default.DDPNumberRangeD;
                typeOfDie = Properties.Settings.Default.DDPColorRangeD;
            }
            else if (totalNumDefenders >= 13 && totalNumDefenders <= 15)
            {
                numDiceToRoll = Properties.Settings.Default.DDPNumberRangeE;
                typeOfDie = Properties.Settings.Default.DDPColorRangeE;
            }
            else // >= 16
            {
                numDiceToRoll = Properties.Settings.Default.DDPNumberRangeF;
                typeOfDie = Properties.Settings.Default.DDPColorRangeF;
            }

            // DWARVES get one additional defense die
            if (m_race == Race.Dwarves)
                numDiceToRoll++;

            int attackersCasualties = 0;
            for (int i = 0; i < numDiceToRoll; ++i)
            {
                Die die = new Die(typeOfDie);
                attackersCasualties += die.roll() ? 1 : 0;
            }
            return attackersCasualties;
        }
        
        // when 'safely' is true, no units passed in will be killed (e.g., casualties will only be applied if a units health is greater than 0)
        // return casualties remaining
        public int MarkForCasualtiesAsManyAsPossible(List<Unit> units, int casualtiesToApply, bool safely)
        {
            int casualtiesRemaining = casualtiesToApply;
            for (int i = 0; i < casualtiesToApply && i < units.Count; ++i)
            {
                Debug.Assert(!units[i].IsDead()); // casualties from previous rounds should have already been removed
                if (!safely || units[i].health > 1)
                {
                    units[i].MarkCasualty();
                    --casualtiesRemaining;
                }
            }
            return casualtiesRemaining;
        }
        
        public void ChooseAndMarkCasualties(int casualtiesToApply)
        {
            // shouldn't be able to call this function when the army is dead
            Debug.Assert(m_units[Unit.Type.Basic].Count > 0 ||
                         m_units[Unit.Type.Advanced].Count > 0 ||
                         m_units[Unit.Type.Special].Count > 0);

            // first, look for 'free casualties', that is, 
            //  only take casualties from units with more than 1 health, 
            //   and in those cases, take EXACTLY 1 casualty. (don't kill the unit)
            bool takeOnlyFreeCasualties = true;
            casualtiesToApply = MarkForCasualtiesAsManyAsPossible(m_units[Unit.Type.Basic], casualtiesToApply, takeOnlyFreeCasualties);
            casualtiesToApply = MarkForCasualtiesAsManyAsPossible(m_units[Unit.Type.Advanced], casualtiesToApply, takeOnlyFreeCasualties);
            casualtiesToApply = MarkForCasualtiesAsManyAsPossible(m_units[Unit.Type.Special], casualtiesToApply, takeOnlyFreeCasualties);

            if (m_race == Race.Elves)
            {
                // Custom logic for Elves' Unity Bonus.  This should be mathed out properly, but for now, Elves will
                // try to take casualties from Basics first, unless that would impact the Unity Bonus, 
                // in which case, casualties will be taken from Advanced if possible to maintain Unity.
                // The Specials are generally too valuable to sacrifice to maintain Unity (IMO).
                // TODO: What do Tim/Cole want to do here?
                int livingBasics = m_units[Unit.Type.Basic].Count;
                int livingAdvanced = m_units[Unit.Type.Advanced].Count;

                int totalNumberUnits = m_units[Unit.Type.Basic].Count + m_units[Unit.Type.Advanced].Count + m_units[Unit.Type.Special].Count;
                int casualtiesRemaining = casualtiesToApply;
                for (int basicsIdx = 0, advancedIdx = 0, specialsIdx = 0, count = 0; 
                     count < casualtiesToApply && count < totalNumberUnits; ++count)
                {
                    if (livingBasics > livingAdvanced)
                    {
                        // we can take a casualty from Basics without impacting Unity
                        Debug.Assert(!m_units[Unit.Type.Basic][basicsIdx].IsDead()); 
                        m_units[Unit.Type.Basic][basicsIdx].MarkCasualty();
                        basicsIdx++; livingBasics--;
                    }
                    else if (livingBasics == livingAdvanced)
                    {
                        // We will only take casualties from Specials if all Basics AND Advanced units are dead.
                        if (livingBasics == 0)
                        {
                            Debug.Assert(!m_units[Unit.Type.Special][specialsIdx].IsDead());
                            m_units[Unit.Type.Special][specialsIdx].MarkCasualty();
                            specialsIdx++;
                        }
                        else
                        {
                            Debug.Assert(!m_units[Unit.Type.Basic][basicsIdx].IsDead());
                            m_units[Unit.Type.Basic][basicsIdx].MarkCasualty();
                            basicsIdx++; livingBasics--;
                        }
                    }
                    else // Advanced units are more numerous than basics
                    {
                        Debug.Assert(!m_units[Unit.Type.Advanced][advancedIdx].IsDead());
                        m_units[Unit.Type.Advanced][advancedIdx].MarkCasualty();
                        advancedIdx++; livingAdvanced--;
                    }

                    --casualtiesRemaining;
                }
            }
            else
            {
                takeOnlyFreeCasualties = false;
                // generally, casualties will be chosen from the lowest attack-valued units first.
                casualtiesToApply = MarkForCasualtiesAsManyAsPossible(m_units[Unit.Type.Basic], casualtiesToApply, takeOnlyFreeCasualties);
                casualtiesToApply = MarkForCasualtiesAsManyAsPossible(m_units[Unit.Type.Advanced], casualtiesToApply, takeOnlyFreeCasualties);
                casualtiesToApply = MarkForCasualtiesAsManyAsPossible(m_units[Unit.Type.Special], casualtiesToApply, takeOnlyFreeCasualties);
            }
        }

        public List<Unit> GetCasualtiesAndRemove()
        {
            List<Unit> ret = new List<Unit>();
            foreach (Unit unit in m_units[Unit.Type.Basic])
            {
                if (unit.IsDead())
                    ret.Add(unit);
            }
            foreach (Unit unit in m_units[Unit.Type.Advanced])
            {
                if (unit.IsDead())
                    ret.Add(unit);
            }
            foreach (Unit unit in m_units[Unit.Type.Special])
            {
                if (unit.IsDead())
                    ret.Add(unit);
            }
            foreach (Unit unit in ret)
            {
                m_units[unit.type].Remove(unit);
            }
            return ret;
        }

        public Unit CreateUnit(Unit.Type type, Race race)
        {
            switch (type)
            {
                case Unit.Type.Basic:
                    switch (race)
                    {
                        case Race.Dwarves: return new Unit(race, Unit.Type.Basic, 
                            Properties.Settings.Default.DwarvesBasicAttack , 
                            Properties.Settings.Default.DwarvesBasicDefense, 3, 0,
                            Properties.Settings.Default.DwarvesBasicHealth);
                        case Race.NightStalkers: return new Unit(race, Unit.Type.Basic, 
                            Properties.Settings.Default.NightStalkersBasicAttack, 
                            Properties.Settings.Default.NightStalkersBasicDefense, 3, 0,
                            Properties.Settings.Default.NightStalkersBasicHealth);
                        case Race.Horde: return new Unit(race, Unit.Type.Basic, 
                            Properties.Settings.Default.HordeBasicAttack, 
                            Properties.Settings.Default.HordeBasicDefense, 3, 0,
                            Properties.Settings.Default.HordeBasicHealth);
                        case Race.Humans: return new Unit(race, Unit.Type.Basic, 
                            Properties.Settings.Default.HumanBasicAttack, 
                            Properties.Settings.Default.HumanBasicDefense, 3, 0,
                            Properties.Settings.Default.HumanBasicHealth);
                        case Race.Elves: return new Unit(race, Unit.Type.Basic,
                            Properties.Settings.Default.ElvesBasicAttack, 
                            Properties.Settings.Default.ElvesBasicDefense, 3, 0,
                            Properties.Settings.Default.ElvesBasicHealth);
                        case Race.Serpents: return new Unit(race, Unit.Type.Basic, 
                            Properties.Settings.Default.SerpentsBasicAttack, 
                            Properties.Settings.Default.SerpentsBasicDefense, 3, 0,
                            Properties.Settings.Default.SerpentsBasicHealth);
                    };break;
                case Unit.Type.Advanced:
                    switch (race)
                    {
                        case Race.Dwarves: return new Unit(race, Unit.Type.Advanced, 
                            Properties.Settings.Default.DwarvesAdvancedAttack,
                            Properties.Settings.Default.DwarvesAdvancedDefense, 
                            4, 1, 
                            Properties.Settings.Default.DwarvesAdvancedHealth);
                        case Race.NightStalkers: return new Unit(race, Unit.Type.Advanced,
                            Properties.Settings.Default.NightStalkersAdvancedAttack,
                            Properties.Settings.Default.NightStalkersAdvancedDefense,
                            4, 1,
                            Properties.Settings.Default.NightStalkersAdvancedHealth);
                        case Race.Horde: return new Unit(race, Unit.Type.Advanced,
                            Properties.Settings.Default.HordeAdvancedAttack,
                            Properties.Settings.Default.HordeAdvancedDefense,
                            4, 1,
                            Properties.Settings.Default.HordeAdvancedHealth);
                        case Race.Humans: return new Unit(race, Unit.Type.Advanced,
                            Properties.Settings.Default.HumanAdvancedAttack,
                            Properties.Settings.Default.HumanAdvancedDefense,
                            4, 1,
                            Properties.Settings.Default.HumanAdvancedHealth);
                        case Race.Elves: return new Unit(race, Unit.Type.Advanced,
                            Properties.Settings.Default.ElvesAdvancedAttack,
                            Properties.Settings.Default.ElvesAdvancedDefense,
                            4, 1,
                            Properties.Settings.Default.ElvesAdvancedHealth);
                        case Race.Serpents: return new Unit(race, Unit.Type.Advanced,
                            Properties.Settings.Default.SerpentsAdvancedAttack,
                            Properties.Settings.Default.SerpentsAdvancedDefense,
                            4, 1,
                            Properties.Settings.Default.SerpentsAdvancedHealth);
                    }; break;
                case Unit.Type.Special:
                    switch (race)
                    {
                        case Race.Dwarves: return new Unit(race, Unit.Type.Special, 
                            Properties.Settings.Default.DwarvesSpecialAttack,
                            Properties.Settings.Default.DwarvesSpecialDefense,
                            0, 4, 
                            Properties.Settings.Default.DwarvesSpecialHealth);
                        case Race.NightStalkers: return new Unit(race, Unit.Type.Special,
                            Properties.Settings.Default.NightStalkersSpecialAttack,
                            Properties.Settings.Default.NightStalkersSpecialDefense,
                            0, 4,
                            Properties.Settings.Default.NightStalkersSpecialHealth);
                        case Race.Horde: return new Unit(race, Unit.Type.Special,
                            Properties.Settings.Default.HordeSpecialAttack,
                            Properties.Settings.Default.HordeSpecialDefense,
                            0, 4,
                            Properties.Settings.Default.HordeSpecialHealth);
                        case Race.Humans: return new Unit(race, Unit.Type.Special,
                            Properties.Settings.Default.HumanSpecialAttack,
                            Properties.Settings.Default.HumanSpecialDefense,
                            0, 4,
                            Properties.Settings.Default.HumanSpecialHealth);
                        case Race.Elves: return new Unit(race, Unit.Type.Special,
                            Properties.Settings.Default.ElvesSpecialAttack,
                            Properties.Settings.Default.ElvesSpecialDefense,
                            0, 4,
                            Properties.Settings.Default.ElvesSpecialHealth);
                        case Race.Serpents: return new Unit(race, Unit.Type.Special,
                            Properties.Settings.Default.SerpentsSpecialAttack,
                            Properties.Settings.Default.SerpentsSpecialDefense,
                            0, 4,
                            Properties.Settings.Default.SerpentsSpecialHealth);
                    }; break;
            }
            Debug.Assert(false, "shouldn't get here");
            return null;
        }


        public static Race RaceStringToRace(string race)
        {
            if (0 == String.Compare(race, "Dwarves", true))
                return Race.Dwarves;
            else if (0 == string.Compare(race, "NightStalkers", true))
                return Race.NightStalkers;
            else if (0 == string.Compare(race, "Horde", true))
                return Race.Horde;
            else if (0 == string.Compare(race, "Humans", true))
                return Race.Humans;
            else if (0 == string.Compare(race, "Elves", true))
                return Race.Elves;
            else if (0 == string.Compare(race, "Serpents", true))
                return Race.Serpents;
            else
            {
                Debug.Assert(false, "couldn't parse race string");
                return Race.Humans;
            }
        }

        public int GetRemainingUnitsOfType(Unit.Type type)
        {
            return m_units[type].Count;
        }

        public int GetTotalSize()
        {
            return m_units[Unit.Type.Basic].Count +
                   m_units[Unit.Type.Advanced].Count +
                   m_units[Unit.Type.Special].Count;
        }

        public Race m_race;
        private CombatManager m_combatManager;
        private Dictionary<Unit.Type, List<Unit>> m_units;
    }
    
}
