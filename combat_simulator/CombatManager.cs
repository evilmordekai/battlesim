﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace combat_simulator
{
    public class CombatProperties
    {
        public bool EnableDefenseDice;
    };

    public class CombatManager
    {
        public enum Player { Player1, Player2, Player3, Player4, Player5, Player6 };
        Dictionary<Player, Army> m_playerToArmy = new Dictionary<Player, Army>();
        public Player m_attackingPlayer = Player.Player1;
        public Player m_defendingPlayer = Player.Player2; 
        public Dictionary<Player, Dictionary<int, List<Unit>>> m_combatResults = new Dictionary<Player, Dictionary<int, List<Unit>>>();
        static public Random m_randomNumberGenerator = new Random();
        public int m_roundNumber = 0;
        public bool m_battleIsOver = false;
        public bool m_enableDefenseDice = true;
        

        public CombatManager(CombatProperties properties)
        {
            m_enableDefenseDice = properties.EnableDefenseDice;
            Init();
        }

        public void Init()
        {
            m_playerToArmy = new Dictionary<Player, Army>();
            m_combatResults = new Dictionary<Player, Dictionary<int, List<Unit>>>();
            m_combatResults[Player.Player1] = new Dictionary<int, List<Unit>>();
            m_combatResults[Player.Player2] = new Dictionary<int, List<Unit>>();
        }

        private Army GetUnitsRemainingForPlayer(Player player)
        {
            return m_playerToArmy[player];
        }
        public Army GetAttackersRemainingUnits() { return GetUnitsRemainingForPlayer(m_attackingPlayer); }
        public Army GetDefendersRemainingUnits() { return GetUnitsRemainingForPlayer(m_defendingPlayer); }

        public Race GetAttackersRace()
        {
            return m_playerToArmy[m_attackingPlayer].m_race;
        }
        public Race GetDefendersRace()
        {
            return m_playerToArmy[m_defendingPlayer].m_race;
        }

        public static string PlayerToName(Player player)
        {
            switch (player)
            {
                case Player.Player1: return "Player 1";
                case Player.Player2: return "Player 2";
                case Player.Player3: return "Player 3";
                case Player.Player4: return "Player 4";
                case Player.Player5: return "Player 5";
                case Player.Player6: return "Player 6";
            }
            Debug.Assert(false, "shouldn't get here");
            return null;
        }

        public void SetupArmies(
            Race attackerRace, 
            int attackerBasics, int attackerAdvanced, int attackerSpecials,
            Race defenderRace,
            int defenderBasics, int defenderAdvanced, int defenderSpecials
            )
        {
            Debug.Assert((attackerBasics > 0 || attackerAdvanced > 0 || attackerSpecials > 0) &&
                          (defenderBasics > 0 || defenderAdvanced > 0 || defenderSpecials > 0));
            m_attackingPlayer = Player.Player1;
            m_defendingPlayer = Player.Player2;
            m_playerToArmy[m_attackingPlayer] = 
                new Army(this, attackerRace, attackerBasics, attackerAdvanced, attackerSpecials);
            m_playerToArmy[m_defendingPlayer] = 
                new Army(this, defenderRace, defenderBasics, defenderAdvanced, defenderSpecials);
        }
        
        private void ComputeCasualties(Player attacker, Player defender)
        {
            bool mayAddDefenseDicePool =
                m_enableDefenseDice &&
                (
                 !(m_playerToArmy[attacker].m_race == Race.Horde
                   && m_playerToArmy[attacker].GetTotalSize() > m_playerToArmy[defender].GetTotalSize()));

            int defendersCasualties = m_playerToArmy[attacker].RollAttackDiceForCombat();
            int attackersCasualties = m_playerToArmy[defender].RollDefenseDiceForCombat(mayAddDefenseDicePool);
            m_playerToArmy[attacker].ChooseAndMarkCasualties(attackersCasualties);
            m_playerToArmy[defender].ChooseAndMarkCasualties(defendersCasualties);
        }

        private void ExecutePreBattlePhase()
        {
            SimulatePreBattlePhase();
            CleanupCasualties(m_roundNumber);
            m_preBattlePhaseHasBeenExecuted = true;
        }

        private bool m_preBattlePhaseHasBeenExecuted = false;
        
        // returns true if battle is over
        private bool SimulateRoundsOrUntilCompletion(int roundsToSimulate)
        {
            // 1 - Pre-battle phase (spy and ranger)
            if (!m_preBattlePhaseHasBeenExecuted)
            {
                ExecutePreBattlePhase();
            }

            // 2 - while battle is NOT over
            int numRoundsExecuted = 0;
            m_battleIsOver = !UnitsRemaining();
            while (!m_battleIsOver && numRoundsExecuted++ < roundsToSimulate)
            {
                ++m_roundNumber;

                //   a - Perform round of combat
                //       dice are rolled, each 'success' becomes a 'casualty'
                //       (normally a player choice, but computer will always take least attack value unit..
                //        with the caveat that Elves will try to maintain their Unity ability bonuses)
                ComputeCasualties(m_attackingPlayer, m_defendingPlayer);

                //   b - Cleanup casualties 
                CleanupCasualties(m_roundNumber);

                m_battleIsOver = !UnitsRemaining();
            }
            return m_battleIsOver;
        }

        public bool SimulateOneRound()
        {
            return SimulateRoundsOrUntilCompletion(1);
        }

        public bool SimulateToCompletion()
        {
            return SimulateRoundsOrUntilCompletion(1000);
        }

        private bool UnitsRemaining()
        {
            foreach (Player player in m_playerToArmy.Keys)
            {
                if (!m_playerToArmy[player].UnitsRemain())
                    return false;
            }
            return true;
        }

        public void CleanupCasualties(int roundNumber)
        {
            foreach (Player player in m_playerToArmy.Keys)
            {
                List<Unit> casulaties = m_playerToArmy[player].GetCasualtiesAndRemove();
                m_combatResults[player].Add(roundNumber, casulaties);
            }
        }

        public void SimulatePreBattlePhase()
        {
            if (m_playerToArmy[m_attackingPlayer].m_race == Race.Elves)
            {
                int numDiceToRoll = m_playerToArmy[m_attackingPlayer].GetRemainingUnitsOfType(Unit.Type.Special);
                DiceType typeOfDie = DiceType.Green;

                int attackersCasualties = 0;
                for (int i = 0; i < numDiceToRoll; ++i)
                {
                    Die die = new Die(typeOfDie);
                    attackersCasualties += die.roll() ? 1 : 0;
                }

                m_playerToArmy[m_defendingPlayer].ChooseAndMarkCasualties(attackersCasualties);
            }
        }
    }
    
}
