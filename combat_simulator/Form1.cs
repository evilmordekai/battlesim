﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;

namespace combat_simulator
{
    public partial class Form1 : Form
    {
        CombatManager battle = null;
        public Form1()
        {
            InitializeComponent();
            edit_attackerBasics.Text = Properties.Settings.Default.AttackerNumBasics.ToString();
            edit_attackerAdvanced.Text = Properties.Settings.Default.AttackerNumAdvanced.ToString();
            edit_attackerSpecials.Text = Properties.Settings.Default.AttackerNumSpecials.ToString();
            edit_defenderBasics.Text = Properties.Settings.Default.DefenderNumBasics.ToString();
            edit_defenderAdvanced.Text = Properties.Settings.Default.DefenderNumAdvanced.ToString();
            edit_defenderSpecials.Text = Properties.Settings.Default.DefenderNumSpecials.ToString();
            combo_attackersRace.Text = Properties.Settings.Default.DefaultAttackerRace.ToString();
            combo_defendersRace.Text = Properties.Settings.Default.DefaultDefenderRace.ToString();
            chk_enableDefenseDice.Checked =
                Properties.Settings.Default.DDPEnabled;
        }

        private void InitGridForSingleBattleSimulation()
        {
            resultsGrid.ColumnCount = 7;
            resultsGrid.Columns[0].Name = "Round";
            resultsGrid.Columns[1].Name = "AttackerBasics";
            resultsGrid.Columns[2].Name = "AttackerAdvanced";
            resultsGrid.Columns[3].Name = "AttackerSpecials";
            resultsGrid.Columns[4].Name = "DefenderBasics";
            resultsGrid.Columns[5].Name = "DefenderAdvanced";
            resultsGrid.Columns[6].Name = "DefenderSpecials";
            resultsGrid.Rows.Clear();
        }

        private void InitGridForMultiBattleSimulation()
        {
            resultsGrid.ColumnCount = 9;
            resultsGrid.Columns[0].Name = "TotalBattlesFought";
            resultsGrid.Columns[1].Name = "AttRace";
            resultsGrid.Columns[2].Name = "AttAvgBasics";
            resultsGrid.Columns[3].Name = "AttAvgAdvanced";
            resultsGrid.Columns[4].Name = "AttAvgSpecials";
            resultsGrid.Columns[5].Name = "DefRace";
            resultsGrid.Columns[6].Name = "DefAvgBasics";
            resultsGrid.Columns[7].Name = "DefAvgAdvanced";
            resultsGrid.Columns[8].Name = "DefAvgSpecials";
            resultsGrid.Rows.Clear();
        }

        private void CountTypesOfUnits(List<Unit> listOfUnits, out int basics, out int advanced, out int specials)
        {
            basics = advanced = specials = 0;
            foreach (Unit unit in listOfUnits)
            {
                switch (unit.type)
                {
                    case Unit.Type.Basic: ++basics;  break;
                    case Unit.Type.Advanced: ++advanced;  break;
                    case Unit.Type.Special: ++specials;  break;
                    default: Debug.Assert(false, "shouldn't get here"); break;
                }
            }
        }

        private void UpdateSingleBattleResultsGrid()
        {
            resultsGrid.Rows.Clear();
            int transpiredRounds = battle.m_roundNumber;
            for (int i = 0; i <= transpiredRounds; ++i)
            {
                string[] arr = new string[7];
                arr[0] = i.ToString();
                foreach (CombatManager.Player player in battle.m_combatResults.Keys)
                {
                    int basics = 0, advanced = 0, specials = 0;
                    CountTypesOfUnits(battle.m_combatResults[player][i], out basics, out advanced, out specials);
                    int basicsIndex = player == battle.m_attackingPlayer ? 1 : 4;
                    int advancedIndex = player == battle.m_attackingPlayer ? 2 : 5;
                    int specialsIndex = player == battle.m_attackingPlayer ? 3 : 6;
                    arr[basicsIndex] = basics.ToString();
                    arr[advancedIndex] = advanced.ToString();
                    arr[specialsIndex] = specials.ToString();
                }
                resultsGrid.Rows.Add(arr);
            }
        }

        private void btn_startSimulation_Click(object sender, EventArgs e)
        {
            Race attackerRace = Army.RaceStringToRace(combo_attackersRace.Text);
            Race defenderRace = Army.RaceStringToRace(combo_defendersRace.Text);

            CombatProperties properties = new CombatProperties();
            properties.EnableDefenseDice = chk_enableDefenseDice.Checked;

            battle = null;
            battle = new CombatManager(properties);
            InitGridForSingleBattleSimulation();
            int attackerBasics = int.Parse(edit_attackerBasics.Text);
            int attackerAdvanced = int.Parse(edit_attackerAdvanced.Text);
            int attackerSpecials = int.Parse(edit_attackerSpecials.Text);
            int defenderBasics = int.Parse(edit_defenderBasics.Text);
            int defenderAdvanced = int.Parse(edit_defenderAdvanced.Text);
            int defenderSpecial = int.Parse(edit_defenderSpecials.Text);
            battle.SetupArmies(attackerRace, attackerBasics, attackerAdvanced, attackerSpecials,
                               defenderRace, defenderBasics, defenderAdvanced, defenderSpecial);

            lbl_casualtyResults.Text = "Casualties by combat round.";
        }

        private void btn_runRound_Click(object sender, EventArgs e)
        {
            if (battle != null)
            {
                bool battleIsOver = battle.SimulateOneRound();
                UpdateSingleBattleResultsGrid();
            }
        }

        private void btn_runToCompletion_Click(object sender, EventArgs e)
        {
            if (battle != null)
            {
                battle.SimulateToCompletion();
                UpdateSingleBattleResultsGrid();
            }
        }

        private struct SurvivingUnits
        {
            public SurvivingUnits(Army _attackers, Army _defenders) { AttackingUnits = _attackers; DefendingUnits = _defenders; }
            public Army DefendingUnits;
            public Army AttackingUnits;
        }
        private List<SurvivingUnits> m_survivingUnits;
        private void btn_simulateAllBattles_Click(object sender, EventArgs e)
        {
            Race attackerRace = Army.RaceStringToRace(combo_attackersRace.Text);
            Race defenderRace = Army.RaceStringToRace(combo_defendersRace.Text);
            m_survivingUnits = new List<SurvivingUnits>();

            // collect input from UX
            int numBattles = int.Parse(edit_numBattles.Text);
            int attackerBasics = int.Parse(edit_attackerBasics.Text);
            int attackerAdvanced = int.Parse(edit_attackerAdvanced.Text);
            int attackerSpecials = int.Parse(edit_attackerSpecials.Text);
            int defenderBasics = int.Parse(edit_defenderBasics.Text);
            int defenderAdvanced = int.Parse(edit_defenderAdvanced.Text);
            int defenderSpecial = int.Parse(edit_defenderSpecials.Text);
            if ((attackerBasics > 0 || attackerAdvanced > 0 || attackerSpecials > 0)
                && (defenderBasics > 0 || defenderAdvanced > 0 || defenderSpecial > 0))
            {
                lbl_casualtyResults.Text = "Surviving Units, averaged over all simulated battles.";

                // track the total remaining units for each type over all simulated battles
                int totalRemainingAttackerBasics = 0;
                int totalRemainingAttackerAdvanced = 0;
                int totalRemainingAttackerSpecials = 0;
                int totalRemainingDefenderBasics = 0;
                int totalRemainingDefenderAdvanced = 0;
                int totalRemainingDefenderSpecials = 0;

                // run the number of simulations specified in the UX
                for (int i = 0; i < numBattles; ++i)
                {
                    CombatProperties properties = new CombatProperties();
                    properties.EnableDefenseDice = chk_enableDefenseDice.Checked;

                    battle = new CombatManager(properties);
                    battle.SetupArmies(attackerRace, attackerBasics, attackerAdvanced, attackerSpecials,
                                       defenderRace, defenderBasics, defenderAdvanced, defenderSpecial);
                    battle.SimulateToCompletion();
                    Army attackersRemainingArmy = battle.GetAttackersRemainingUnits();
                    Army defendersRemainingArmy = battle.GetDefendersRemainingUnits();
                    m_survivingUnits.Add(new SurvivingUnits(attackersRemainingArmy, defendersRemainingArmy));

                    totalRemainingAttackerBasics += attackersRemainingArmy.GetRemainingUnitsOfType(Unit.Type.Basic);
                    totalRemainingAttackerAdvanced += attackersRemainingArmy.GetRemainingUnitsOfType(Unit.Type.Advanced);
                    totalRemainingAttackerSpecials += attackersRemainingArmy.GetRemainingUnitsOfType(Unit.Type.Special);
                    totalRemainingDefenderBasics += defendersRemainingArmy.GetRemainingUnitsOfType(Unit.Type.Basic);
                    totalRemainingDefenderAdvanced += defendersRemainingArmy.GetRemainingUnitsOfType(Unit.Type.Advanced);
                    totalRemainingDefenderSpecials += defendersRemainingArmy.GetRemainingUnitsOfType(Unit.Type.Special);
                }

                // init the column/headers for the results grid display
                InitGridForMultiBattleSimulation();

                // calculate the average remaining units over the executed simulated battles and update results grid display
                string[] arr = new string[9];
                arr[0] = numBattles.ToString();
                arr[1] = battle.GetAttackersRace().ToString();
                arr[2] = (totalRemainingAttackerBasics / (float)numBattles).ToString();
                arr[3] = (totalRemainingAttackerAdvanced / (float)numBattles).ToString();
                arr[4] = (totalRemainingAttackerSpecials / (float)numBattles).ToString();
                arr[5] = battle.GetDefendersRace().ToString();
                arr[6] = (totalRemainingDefenderBasics / (float)numBattles).ToString();
                arr[7] = (totalRemainingDefenderAdvanced / (float)numBattles).ToString();
                arr[8] = (totalRemainingDefenderSpecials / (float)numBattles).ToString();
                resultsGrid.Rows.Add(arr);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (m_survivingUnits != null &&
               m_survivingUnits.Count > 0)
            {
                string filename = string.Format("battle_{0}.txt", DateTime.Now.ToString("yyyy-dd-M--HH-mm-ss"));
                using (StreamWriter writetext = new StreamWriter(filename))
                {
                    string line = $"ATTACKERS\t{Army.RaceStringToRace(combo_attackersRace.Text)}\tDEFENDERS\t{Army.RaceStringToRace(combo_defendersRace.Text)}\tBATTLES SIMULATED\t{edit_numBattles.Text}";
                    writetext.WriteLine(line);
                    line = $"STARTING\tATTACKERS\tARMY\t\r\nBASICS\t{edit_attackerBasics.Text}\tADVANCED\t{edit_attackerAdvanced.Text}\tSPECIALS\t{edit_attackerSpecials.Text}";
                    writetext.WriteLine(line);
                    line = $"STARTING\tDEFENDERS\tARMY\t\r\nBASICS\t{edit_defenderBasics.Text}\tADVANCED\t{edit_defenderAdvanced.Text}\tSPECIALS\t{edit_defenderSpecials.Text}";
                    writetext.WriteLine(line);
                    writetext.WriteLine();
                    writetext.WriteLine();
                    line = $"SURVIVING\tUNITS\tPER\tBATTLE\r\n";
                    writetext.WriteLine(line);
                    int battle = 0;
                    foreach (SurvivingUnits survivors in m_survivingUnits)
                    {
                        line =
                            $"BATTLE\t{battle + 1}" +
                            $"\tATTBASICS\t{m_survivingUnits[battle].AttackingUnits.GetRemainingUnitsOfType(Unit.Type.Basic)}" +
                            $"\tATTADVANCED\t{m_survivingUnits[battle].AttackingUnits.GetRemainingUnitsOfType(Unit.Type.Advanced)}" +
                            $"\tATTSPECIALS\t{m_survivingUnits[battle].AttackingUnits.GetRemainingUnitsOfType(Unit.Type.Special)}" +
                            $"\tDEFBASICS\t{m_survivingUnits[battle].DefendingUnits.GetRemainingUnitsOfType(Unit.Type.Basic)}" +
                            $"\tDEFADVANCED\t{m_survivingUnits[battle].DefendingUnits.GetRemainingUnitsOfType(Unit.Type.Advanced)}" +
                            $"\tDEFSPECIALS\t{m_survivingUnits[battle].DefendingUnits.GetRemainingUnitsOfType(Unit.Type.Special)}";
                        writetext.WriteLine(line);
                        battle++;
                    }
                }
            }
        }
    }
}
