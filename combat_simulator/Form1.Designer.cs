﻿namespace combat_simulator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_startSimulation = new System.Windows.Forms.Button();
            this.btn_runRound = new System.Windows.Forms.Button();
            this.btn_runToCompletion = new System.Windows.Forms.Button();
            this.resultsGrid = new System.Windows.Forms.DataGridView();
            this.lbl_casualtyResults = new System.Windows.Forms.Label();
            this.edit_attackerBasics = new System.Windows.Forms.TextBox();
            this.edit_attackerAdvanced = new System.Windows.Forms.TextBox();
            this.edit_attackerSpecials = new System.Windows.Forms.TextBox();
            this.edit_defenderBasics = new System.Windows.Forms.TextBox();
            this.edit_defenderAdvanced = new System.Windows.Forms.TextBox();
            this.edit_defenderSpecials = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.btn_simulateAllBattles = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.edit_numBattles = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.combo_attackersRace = new System.Windows.Forms.ComboBox();
            this.combo_defendersRace = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.chk_enableDefenseDice = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.resultsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_startSimulation
            // 
            this.btn_startSimulation.Location = new System.Drawing.Point(718, 17);
            this.btn_startSimulation.Name = "btn_startSimulation";
            this.btn_startSimulation.Size = new System.Drawing.Size(142, 23);
            this.btn_startSimulation.TabIndex = 0;
            this.btn_startSimulation.Text = "Start/Restart Simulation";
            this.btn_startSimulation.UseVisualStyleBackColor = true;
            this.btn_startSimulation.Click += new System.EventHandler(this.btn_startSimulation_Click);
            // 
            // btn_runRound
            // 
            this.btn_runRound.Location = new System.Drawing.Point(718, 46);
            this.btn_runRound.Name = "btn_runRound";
            this.btn_runRound.Size = new System.Drawing.Size(142, 23);
            this.btn_runRound.TabIndex = 1;
            this.btn_runRound.Text = "Run Round";
            this.btn_runRound.UseVisualStyleBackColor = true;
            this.btn_runRound.Click += new System.EventHandler(this.btn_runRound_Click);
            // 
            // btn_runToCompletion
            // 
            this.btn_runToCompletion.Location = new System.Drawing.Point(718, 73);
            this.btn_runToCompletion.Name = "btn_runToCompletion";
            this.btn_runToCompletion.Size = new System.Drawing.Size(142, 23);
            this.btn_runToCompletion.TabIndex = 3;
            this.btn_runToCompletion.Text = "Run To Completion";
            this.btn_runToCompletion.UseVisualStyleBackColor = true;
            this.btn_runToCompletion.Click += new System.EventHandler(this.btn_runToCompletion_Click);
            // 
            // resultsGrid
            // 
            this.resultsGrid.AllowUserToAddRows = false;
            this.resultsGrid.AllowUserToDeleteRows = false;
            this.resultsGrid.AllowUserToOrderColumns = true;
            this.resultsGrid.AllowUserToResizeRows = false;
            this.resultsGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.resultsGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.resultsGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.resultsGrid.Location = new System.Drawing.Point(0, 274);
            this.resultsGrid.Name = "resultsGrid";
            this.resultsGrid.Size = new System.Drawing.Size(884, 273);
            this.resultsGrid.TabIndex = 4;
            // 
            // lbl_casualtyResults
            // 
            this.lbl_casualtyResults.AutoSize = true;
            this.lbl_casualtyResults.Location = new System.Drawing.Point(0, 258);
            this.lbl_casualtyResults.Name = "lbl_casualtyResults";
            this.lbl_casualtyResults.Size = new System.Drawing.Size(85, 13);
            this.lbl_casualtyResults.TabIndex = 5;
            this.lbl_casualtyResults.Text = "Casualty Results";
            // 
            // edit_attackerBasics
            // 
            this.edit_attackerBasics.Location = new System.Drawing.Point(106, 109);
            this.edit_attackerBasics.Name = "edit_attackerBasics";
            this.edit_attackerBasics.Size = new System.Drawing.Size(35, 20);
            this.edit_attackerBasics.TabIndex = 6;
            this.edit_attackerBasics.Text = "100";
            // 
            // edit_attackerAdvanced
            // 
            this.edit_attackerAdvanced.Location = new System.Drawing.Point(106, 136);
            this.edit_attackerAdvanced.Name = "edit_attackerAdvanced";
            this.edit_attackerAdvanced.Size = new System.Drawing.Size(35, 20);
            this.edit_attackerAdvanced.TabIndex = 7;
            this.edit_attackerAdvanced.Text = "50";
            // 
            // edit_attackerSpecials
            // 
            this.edit_attackerSpecials.Location = new System.Drawing.Point(106, 163);
            this.edit_attackerSpecials.Name = "edit_attackerSpecials";
            this.edit_attackerSpecials.Size = new System.Drawing.Size(35, 20);
            this.edit_attackerSpecials.TabIndex = 8;
            this.edit_attackerSpecials.Text = "10";
            // 
            // edit_defenderBasics
            // 
            this.edit_defenderBasics.Location = new System.Drawing.Point(359, 109);
            this.edit_defenderBasics.Name = "edit_defenderBasics";
            this.edit_defenderBasics.Size = new System.Drawing.Size(35, 20);
            this.edit_defenderBasics.TabIndex = 9;
            this.edit_defenderBasics.Text = "100";
            // 
            // edit_defenderAdvanced
            // 
            this.edit_defenderAdvanced.Location = new System.Drawing.Point(359, 136);
            this.edit_defenderAdvanced.Name = "edit_defenderAdvanced";
            this.edit_defenderAdvanced.Size = new System.Drawing.Size(35, 20);
            this.edit_defenderAdvanced.TabIndex = 10;
            this.edit_defenderAdvanced.Text = "50";
            // 
            // edit_defenderSpecials
            // 
            this.edit_defenderSpecials.Location = new System.Drawing.Point(359, 162);
            this.edit_defenderSpecials.Name = "edit_defenderSpecials";
            this.edit_defenderSpecials.Size = new System.Drawing.Size(35, 20);
            this.edit_defenderSpecials.TabIndex = 11;
            this.edit_defenderSpecials.Text = "10";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(94, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 13);
            this.label2.TabIndex = 12;
            this.label2.Text = "Attacker";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(343, 66);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 13;
            this.label3.Text = "Defender";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(44, 112);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "Basics";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(44, 139);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Advanced";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 165);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Specials";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(297, 112);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 17;
            this.label7.Text = "Basics";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(297, 139);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Advanced";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(297, 166);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(47, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Specials";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(106, 9);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(231, 31);
            this.label10.TabIndex = 20;
            this.label10.Text = "Two Guys Games";
            // 
            // btn_simulateAllBattles
            // 
            this.btn_simulateAllBattles.Location = new System.Drawing.Point(718, 175);
            this.btn_simulateAllBattles.Name = "btn_simulateAllBattles";
            this.btn_simulateAllBattles.Size = new System.Drawing.Size(142, 23);
            this.btn_simulateAllBattles.TabIndex = 21;
            this.btn_simulateAllBattles.Text = "Simulate All Battles";
            this.btn_simulateAllBattles.UseVisualStyleBackColor = true;
            this.btn_simulateAllBattles.Click += new System.EventHandler(this.btn_simulateAllBattles_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 23);
            this.label1.TabIndex = 24;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(584, 130);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(121, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Simulate Multiple Battles";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(584, 27);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(114, 13);
            this.label12.TabIndex = 25;
            this.label12.Text = "Simulate Single Battles";
            // 
            // edit_numBattles
            // 
            this.edit_numBattles.Location = new System.Drawing.Point(718, 149);
            this.edit_numBattles.Name = "edit_numBattles";
            this.edit_numBattles.Size = new System.Drawing.Size(100, 20);
            this.edit_numBattles.TabIndex = 26;
            this.edit_numBattles.Text = "100";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(825, 154);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 27;
            this.label13.Text = "# Battles";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(297, 86);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 13);
            this.label14.TabIndex = 31;
            this.label14.Text = "Race";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(44, 86);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(33, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "Race";
            // 
            // combo_attackersRace
            // 
            this.combo_attackersRace.FormattingEnabled = true;
            this.combo_attackersRace.Items.AddRange(new object[] {
            "Dwarves",
            "NightStalkers",
            "Horde",
            "Humans",
            "Elves",
            "Serpents"});
            this.combo_attackersRace.Location = new System.Drawing.Point(106, 82);
            this.combo_attackersRace.Name = "combo_attackersRace";
            this.combo_attackersRace.Size = new System.Drawing.Size(121, 21);
            this.combo_attackersRace.TabIndex = 32;
            this.combo_attackersRace.Text = "Humans";
            // 
            // combo_defendersRace
            // 
            this.combo_defendersRace.FormattingEnabled = true;
            this.combo_defendersRace.Items.AddRange(new object[] {
            "Dwarves",
            "NightStalkers",
            "Horde",
            "Humans",
            "Elves",
            "Serpents"});
            this.combo_defendersRace.Location = new System.Drawing.Point(359, 83);
            this.combo_defendersRace.Name = "combo_defendersRace";
            this.combo_defendersRace.Size = new System.Drawing.Size(121, 21);
            this.combo_defendersRace.TabIndex = 33;
            this.combo_defendersRace.Text = "Humans";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(718, 205);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 34;
            this.button1.Text = "Export";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // chk_enableDefenseDice
            // 
            this.chk_enableDefenseDice.AutoSize = true;
            this.chk_enableDefenseDice.Location = new System.Drawing.Point(196, 210);
            this.chk_enableDefenseDice.Name = "chk_enableDefenseDice";
            this.chk_enableDefenseDice.Size = new System.Drawing.Size(127, 17);
            this.chk_enableDefenseDice.TabIndex = 35;
            this.chk_enableDefenseDice.Text = "Enable Defense Dice";
            this.chk_enableDefenseDice.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 547);
            this.Controls.Add(this.chk_enableDefenseDice);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.combo_defendersRace);
            this.Controls.Add(this.combo_attackersRace);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.edit_numBattles);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_simulateAllBattles);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.edit_defenderSpecials);
            this.Controls.Add(this.edit_defenderAdvanced);
            this.Controls.Add(this.edit_defenderBasics);
            this.Controls.Add(this.edit_attackerSpecials);
            this.Controls.Add(this.edit_attackerAdvanced);
            this.Controls.Add(this.edit_attackerBasics);
            this.Controls.Add(this.lbl_casualtyResults);
            this.Controls.Add(this.resultsGrid);
            this.Controls.Add(this.btn_runToCompletion);
            this.Controls.Add(this.btn_runRound);
            this.Controls.Add(this.btn_startSimulation);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.resultsGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_startSimulation;
        private System.Windows.Forms.Button btn_runRound;
        private System.Windows.Forms.Button btn_runToCompletion;
        private System.Windows.Forms.DataGridView resultsGrid;
        private System.Windows.Forms.Label lbl_casualtyResults;
        private System.Windows.Forms.TextBox edit_attackerBasics;
        private System.Windows.Forms.TextBox edit_attackerAdvanced;
        private System.Windows.Forms.TextBox edit_attackerSpecials;
        private System.Windows.Forms.TextBox edit_defenderBasics;
        private System.Windows.Forms.TextBox edit_defenderAdvanced;
        private System.Windows.Forms.TextBox edit_defenderSpecials;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_simulateAllBattles;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox edit_numBattles;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox combo_attackersRace;
        private System.Windows.Forms.ComboBox combo_defendersRace;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chk_enableDefenseDice;
    }
}

