﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace combat_simulator
{
    public class Unit
    {
        public Unit(
            Race race, 
            Type _type, 
            int _attack, 
            int _defense, 
            int _goldCost, 
            int _resourceCost, 
            int _health)
        {
            type = _type;
            name = getUnitName(type, race);
            attack = _attack;
            defense = _defense;
            goldCost = _goldCost;
            resourceCost = _resourceCost;
            health = _health;
        }
        public int attack;
        public int defense;
        public int goldCost;
        public int resourceCost;
        public Type type;
        public string name;
        public int health;

        public enum Type
        {
            Basic, Advanced, Special
        };

        public bool IsDead()  
        {
            return health <= 0;
        }

        public void MarkCasualty()
        {
            health--;
        }

        public static string getUnitName(Unit.Type type, Race race)
        {
            switch (race)
            {
                case Race.Dwarves:
                    switch (type)
                    {
                        case Unit.Type.Basic: return "Dwarf";
                        case Unit.Type.Advanced: return "Chariot";
                        case Unit.Type.Special: return "Catapult";
                    }break;
                case Race.NightStalkers:
                    switch (type)
                    {
                        case Unit.Type.Basic: return "Zombie";
                        case Unit.Type.Advanced: return "Ghoul";
                        case Unit.Type.Special: return "Lich";
                    }
                    break;
                case Race.Horde:
                    switch (type)
                    {
                        case Unit.Type.Basic: return "Orc";
                        case Unit.Type.Advanced: return "Giant";
                        case Unit.Type.Special: return "Pillager";
                    }
                    break;
                case Race.Humans:
                    switch (type)
                    {
                        case Unit.Type.Basic: return "Knight";
                        case Unit.Type.Advanced: return "Calvary";
                        case Unit.Type.Special: return "Spy";
                    }
                    break;
                case Race.Elves:
                    switch (type)
                    {
                        case Unit.Type.Basic: return "Warrior";
                        case Unit.Type.Advanced: return "Rider";
                        case Unit.Type.Special: return "Ranger";
                    }
                    break;
                case Race.Serpents:
                    switch (type)
                    {
                        case Unit.Type.Basic: return "Lizardman";
                        case Unit.Type.Advanced: return "Salamander";
                        case Unit.Type.Special: return "Giant Worm";
                    }
                    break;
            }
            Debug.Assert(false, "shouldn't get here");
            return null;
        }
    }

    

}
